var listOfAllUser = undefined;

var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        let data = JSON.parse(xmlhttp.responseText);
        listOfAllUser = data;
    }
};
xmlhttp.open("GET", "https://online-lectures-cs.thi.de/chat/cd994c31-b7e1-41ba-a20a-43a64469bc49/user", true);
// Add token, e. g., from Tom
xmlhttp.setRequestHeader('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiVG9tIiwiaWF0IjoxNjM2NTUzNDI5fQ.fLQDr8-40NEA2OvohJ0LEJOsd-NsoQKGqbOoCAUHdFo');
xmlhttp.send();



function findFriends(element) {

    if(listOfAllUser === undefined) {
        alert('Something went wrong! Try again later!');
        return;
    }

    if(element instanceof HTMLInputElement) {
        const typedValue = element.value;
        const listOfGuessFriends = [];
        const guessTable = document.getElementById('guessTable');
        guessTable.innerHTML = '';

        for(let user of listOfAllUser) {
            if(typedValue !== '' && user.startsWith(typedValue) && user !== typedValue) {
                listOfGuessFriends.push(user);
            }
        }

        for(guessFriend of listOfGuessFriends) {
            
            const tableRow = document.createElement('tr');
            const tableElement = document.createElement('td');
            tableElement.innerText = guessFriend;
            tableElement.onclick = () => {
                element.value = tableElement.innerText;
                findFriends(element);
            };
            tableRow.appendChild(tableElement);
            guessTable.appendChild(tableRow);

            i++;
        }
    }
}

function addFriend(inputBarId) {
    const input = document.getElementById(inputBarId);
    const typedName = input.value;
    if(listOfAllUser === undefined) {
        alert('Something is wrong! Try again later!');
    } 
    else {
        if(!listOfAllUser.includes(typedName)) {
            alert('User does not exist!');
        }
        else {
            alert('Friend would be added ^_^');
        }
    }
}