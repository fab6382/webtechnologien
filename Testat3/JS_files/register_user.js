// Globale Variablen
var username;
var pass;
var pass2;
var valid;
var xmlhttp = new XMLHttpRequest();

// Wenn das Fenster geladen wird, wird diese Funktion setGlobalVariables() ausgeführt
window.addEventListener("load", function(event) {
    setGlobalVariables();
});

// Globalen Variablen wird ein Wert zugewiesen
function setGlobalVariables() {
    this.valid = false;
    this.username = document.getElementById('register_username');
    this.pass = document.getElementById('register_password_one');
    this.pass2 = document.getElementById('register_password_two');
}

// AJAX Serververbindung
xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState === 4) {
        if(xmlhttp.status === 204) {
            valid = false;
            setInputBorder();
        } else if(xmlhttp.status === 404) {
            valid = true;
            setInputBorder();
        }
    }
};

/**  Überprüfung des Nutzernamen auf:
 *      - Verfügbarkeit
 *      - Länge
 *  --> Setzen der BorderColor je nach Ergebnis
*/
function setInputBorder() {   
    if(valid === false || username.value.length < 3) {
        username.style.borderColor = 'red';
        valid = false; // Username kann zwar verfügbar sein, aber die Bedingung der Länge nicht erfüllen --> Überprüfung in createButton() auf valid!
        return false;
    } else{
        username.style.borderColor = 'green';
        return true;
    }
}

// Erster Teil des Username-Überprüfungs Prozesses.
// --> Daten werden geholt und eine entsprechende Anfrage an den Server geschickt
function username_checkInput() {
    let link = "https://online-lectures-cs.thi.de/chat/ae2cc78e-2a10-4216-bf6e-db5c4d7af328/user/";

    if(username.value === '') {
        username.style.borderColor = 'grey';
        return;
    }

    link = link + username.value;
    xmlhttp.open("GET", link, true);
    xmlhttp.send();
}

// Überprüfung des ersten + Confirm Passwortes auf Richtigkeit
// --> Confirm Passwort, da zuerst dessen Input ausgefüllt werden kann und sobald Input des ersten Passwortes ausgefüllt ist, werden beide Inhalte nochmal miteinander verglichen

function password_checkInput() {
    if(pass.value === '') {
        pass.style.borderColor = 'grey';
        this.password_two_checkInput();
        return false;
    }

    if(pass.value.length < 8) {
        pass.style.borderColor = 'red';
        this.password_two_checkInput();
        return false;
    }
    else{
        pass.style.borderColor = 'green';
        this.password_two_checkInput();
        return true;
    }
}

// Überprürung des Confirm Passwortes auf Richtigkeit
function password_two_checkInput() {

    if(pass2.value === '') {
        pass2.style.borderColor = 'grey';
        return false;
    }

    if(pass.value !== pass2.value || pass2.value.length < 8) {
        
        pass2.style.borderColor = 'red';
        return false;
    }
    else{
        pass2.style.borderColor = 'green';
        return true;
    }
}

// Erneute Überprüfung bei drückend des Create-Button
// --> gibt false-Wert zurück, um Weiterleitung zu friends.html bei fehlerhaftem Input zu verhindern
function createButton() {  
    const passwordCheck = password_checkInput();
    const secondPasswordCheck = password_two_checkInput();

    if (valid === false || passwordCheck === false || secondPasswordCheck === false) {
        if(valid === false) {
            username.style.borderColor = 'red';
        }

        if(passwordCheck === false) {
            pass.style.borderColor = 'red';
        }

        if(secondPasswordCheck === false) {
            pass2.style.borderColor = 'red';
        }

        return false;
    }
    else {
        return true;
    }
}