//Collection ID und Token, damit kein Zugriff auf die Dummy-Seite notwendig ist
var collectionID = "ec9338db-51af-42e2-ae0f-8fb5d7f50862";
var tokenTom = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiVG9tIiwiaWF0IjoxNjM2NjQxNzAyfQ.cojvhapWdz90p_rHdtOot1fce_crv9O7hS0wtZHw0fQ";
var tokenJerry = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiSmVycnkiLCJpYXQiOjE2MzY2NDE3MDJ9.yKXmJTlpdGSTM585efwxc8luy1FGX6eDOdvsNZuaFrM";

//Send Messages definiert
let xmlhttpsend = new XMLHttpRequest();
xmlhttpsend.onreadystatechange = function () {
    if (xmlhttpsend.readyState == 4 && xmlhttpsend.status == 204) {
        console.log("done...");
    }
};

//List Messages definiert
var messages = 0;
var previousMessages = 0;

var xmlhttplist = new XMLHttpRequest();
xmlhttplist.onreadystatechange = function () {
    if (xmlhttplist.readyState == 4 && xmlhttplist.status == 200) {
        let data = JSON.parse(xmlhttplist.responseText);

        messages = data.length;

        //Für alle neuen Nachrichten iteriere durch Array aller jemals gesendeten Nachrichten und füge als neues Paragraph-Element ein
        for (i = previousMessages; i < messages; i++) {

            var time = new Date(data[i].time).toLocaleTimeString();

            console.log(time);

            const paragraph = document.createElement("p");
            paragraph.className = "chat_message";
            const rightSpan = document.createElement("span");
            rightSpan.className = "chat_message_time";
            const leftText = document.createTextNode(`${data[i].from}: ${data[i].msg}`);
            const rightText = document.createTextNode(time);
            rightSpan.appendChild(rightText);
            paragraph.appendChild(leftText);
            paragraph.appendChild(rightSpan);

            const element = document.getElementById("chat");
            element.prepend(paragraph);
        }

        previousMessages = messages;

    }
};

//Ausführen, sobald das Fenster geladen hat
window.onload = function () {

    //Alle bisherigen Nachrichten anzeigen
    xmlhttplist.open("GET", "https://online-lectures-cs.thi.de/chat/ec9338db-51af-42e2-ae0f-8fb5d7f50862/message/Tom", true);
    xmlhttplist.setRequestHeader('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiSmVycnkiLCJpYXQiOjE2MzY2NDE3MDJ9.yKXmJTlpdGSTM585efwxc8luy1FGX6eDOdvsNZuaFrM');
    xmlhttplist.send();

    //Drücken der Enter-Taste im Input-Feld zum Button-Click umwandeln
    document.getElementById("message_input").addEventListener("keyup", function (event) {

        if (event.key === "Enter") {
            document.getElementById("message_button").click();
        }

    });

    //Beim Drücken des Senden-Buttons die Nachricht aus dem Input-Feld absenden und das Input-Feld danach leeren
    document.getElementById("message_button").addEventListener("click", function () {

        xmlhttpsend.open("POST", "https://online-lectures-cs.thi.de/chat/ec9338db-51af-42e2-ae0f-8fb5d7f50862/message", true);
        xmlhttpsend.setRequestHeader('Content-type', 'application/json');
        xmlhttpsend.setRequestHeader('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiSmVycnkiLCJpYXQiOjE2MzY2NDE3MDJ9.yKXmJTlpdGSTM585efwxc8luy1FGX6eDOdvsNZuaFrM');

        let data = {
            message: document.getElementById("message_input").value,
            to: "Tom"
        };
        let jsonString = JSON.stringify(data);
        xmlhttpsend.send(jsonString);

        document.getElementById("message_input").value = null;

    });
}

//Jede Sekunde aktualisieren
window.setInterval(function () {
    xmlhttplist.open("GET", "https://online-lectures-cs.thi.de/chat/ec9338db-51af-42e2-ae0f-8fb5d7f50862/message/Tom", true);
    xmlhttplist.setRequestHeader('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiSmVycnkiLCJpYXQiOjE2MzY2NDE3MDJ9.yKXmJTlpdGSTM585efwxc8luy1FGX6eDOdvsNZuaFrM');
    xmlhttplist.send();
}, 1000);