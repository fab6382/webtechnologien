<?php
    spl_autoload_register(function($class) {
        include str_replace('\\', '/', $class) . '.php';
    });

    session_start();

    define('CHAT_SERVER_URL', 'https://online-lectures-cs.thi.de/chat/');
    define('CHAT_SERVER_ID', 'cd994c31-b7e1-41ba-a20a-43a64469bc49');

    $service = new Utils\BackendService(CHAT_SERVER_URL, CHAT_SERVER_ID);
?>