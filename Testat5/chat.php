<?php
    require("start.php");
    
    if(isset($_GET['chatname'])) {
        $chatuser = $_GET['chatname'];
        
        if(isset($_SESSION['user'])) {

        } else {
            header('Location: login.php');
        }
    } else {
        header('Location: friends.php');
    }
?>

<!DOCTYPE html>

<html>
    <head>
        <title> Chat </title>
        <link rel="stylesheet" href="style.css">

        <script>
            window.chatToken = "<?= $_SESSION['chat_token'] ?>";
            window.chatCollectionId = "<?= CHAT_SERVER_ID ?>";
            window.chatServer = "<?= CHAT_SERVER_URL ?>";
            window.chatUser = "<?= $chatuser ?>";
        </script>

        <script src="JS_files/chat.js"></script>
    </head>

    <body class="simple_centered_element">
        <h1 class="left_sided_header"><?php echo "Chat with " . $chatuser?></h1>

        <div class="link_bar">
            <a href="friends.php"> &lt; Back </a>
            |
            <a href="profile.php?friend=<?php echo $chatuser ?>"> Profile </a>
            |
            <a id="remove" href="friends.php?remove=<?php echo $chatuser ?>"> Remove Friend </a>
        </div>


        <div id="cut_line_chat">

            <span class="information_container">

                <div id="chat">

                </div>

            </span>

        </div>
        
        <div class="input_bar">
            <input type="text" id="message_input" placeholder="New Message" autocomplete="off">
            <button id="message_button"> Send </button>
        </div>
    </body>
</html>