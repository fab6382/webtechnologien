<?php
    namespace Utils;

    class BackendService {
        private $base;
        private $id;

        public function __construct($base, $id) {
            $this->base = $base;
            $this->id = $id;
        }

        public function test() {
            try {
                return HttpClient::get($this->base . '/test.json');
            } catch(\Exception $e) {
                error_log($e);
            }
            return false;
        }

        public function login($username, $password) {
            try {
                $result = HttpClient::post($this->base . $this->id . '/login', array("username" => $username, "password" => $password));
                $_SESSION["chat_token"] = $result->token;
                return true;
            } catch(\Exception $e) {
                error_log($e);
            }
            return false;
        }

        public function register($username, $password) {
            try {
                $result = HttpClient::post($this->base . $this->id . '/register', array("username" => $username, "password" => $password));
                $_SESSION["chat_token"] = $result->token;
                return true;
            } catch(\Exception $e) {
                error_log($e);
            }
            return false;
        }

        public function loadUser($username) {
            try {
                $data = HttpClient::get($this->base . $this->id . '/user/' . $username, $_SESSION["chat_token"]);
                return \Model\User::fromJson($data);
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function saveUser($user) {
            try {
                return HttpClient::post($this->base . $this->id . '/user/' . $user->getUsername(), $user, $_SESSION["chat_token"]);
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function loadFriends() {
            // try {
            //     $data = HttpClient::post($this->base . $this->id . '/friend', $_SESSION["chat_token"]);
            //     $friendList = array();
            //     foreach ($data as $key => $user) {
            //         $userCopy = null;
            //         $userCopy = \Model\Friend::fromJson($user);
            //         array_push($friendList, $userCopy);
            //     }
            //     json_encode($friendList);
            //     return $friendList;
            // } catch(\Exception $e) {
            //     error_log($e);
            // }
            try {
                $data = HttpClient::get($this->base . $this->id . '/friend', $_SESSION["chat_token"]);
                $friendList = array();
                foreach ($data as $key => $user) {
                    $userCopy = null;
                    $userCopy = \Model\Friend::fromJson($user);
                    array_push($friendList, $userCopy);
                }
                //json_encode($friendList);
                return $friendList;
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function friendRequest($friend) {
            try {
                HttpClient::post($this->base . $this->id . '/friend', array("username" => $friend->getUsername()), $_SESSION["chat_token"]);
                return true;
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function friendAccept($friend) {
            try {
                $friend->setStatusAccepted();
                return HttpClient::put($this->base . $this->id . '/friend/' . $friend->getUsername(), array("status" => $friend->getStatus()), $_SESSION["chat_token"]);
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function friendDismiss($friend) {
            try {
                $friend->setStatusDismissed();
                return HttpClient::put($this->base . $this->id . '/friend/' . $friend->getUsername(), array("status" => $friend->getStatus()), $_SESSION["chat_token"]);
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function friendRemove($friend) {
            try {
                return HttpClient::delete($this->base . $this->id . '/friend/' . $friend->getUsername(), $_SESSION["chat_token"]);
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function userExists($username) {
            try {
                return HttpClient::get($this->base . $this->id . '/user/' . $username, $_SESSION["chat_token"]);
            } catch(\Exception $e) {
                error_log($e);
            }
        }

        public function getUnread() {
            try {
                return HttpClient::get($this->base . $this->id . '/unread', $_SESSION["chat_token"]);
            } catch(\Exception $e) {
                error_log($e);
            }
        }
    }
?>