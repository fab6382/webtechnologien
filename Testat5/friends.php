<?php
    require("start.php");

    $errorMsg = false;

    if(!isset($_SESSION["user"])) {
        header("Location: login.php");
    }
    else {
        if(isset($_GET["remove"])) {
            $service->friendRemove($service->loadUser($_GET["remove"]));
        }
        if(isset($_POST["send_request"])) {
            if($service->friendRequest($service->loadUser($_POST["requested_user_name"])) !== true) {
                $errorMsg = true;
            }
        }
        if(isset($_GET["add_request"])) {
            $toBeAddedFriend = new Model\Friend($_GET["add_request"]);
            $toBeAddedFriend->setStatusAccepted();
            $service->friendAccept($toBeAddedFriend);
        }
        if(isset($_GET["decline_request"])) {
            $toBeDeclinedFriend = new Model\Friend($_GET["decline_request"]);
            $service->friendDismiss($toBeDeclinedFriend);
        }
        $friends = $service->loadFriends();
        $unreadMessages = $service->getUnread();
        $accepted = array();
        $requested = array();
        foreach($friends as $friend) {
            if($friend->getStatus() == "accepted") {
                array_push($accepted, $friend->getUsername());
            }
            else {
                array_push($requested, $friend->getUsername());
            }
        }
    }
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Friends</title>
        <link rel="stylesheet" href="style.css">
        <script>
            window.chatToken = "<?= $_SESSION['chat_token'] ?>";
            window.chatCollectionId = "<?= CHAT_SERVER_ID ?>";
            window.chatServer = "<?= CHAT_SERVER_URL ?>";
        </script>
        <script src="JS_files/find_friends.js"></script> 
    </head>

    <body class="simple_centered_element">
        <h1 class="left_sided_header"><?php echo $_SESSION["user"] . "'s Friends"?></h1>


        <div class="link_bar">
            <a href="logout.php">&lt;Logout</a>
            |
            <a href="settings.php">Settings</a>
        </div>

        <div id="cut_line_friends">
            
            <span class="information_container">
                <div id="friends">

                    <ul id="numeration">
                        <?php
                            if(count($accepted) == 0) {
                                echo "<li>Bisher hast du keine Freunde :(</li>";
                            }
                            else {
                                for($i = 0; $i < count($accepted); $i = $i + 1) {
                                    $name = $accepted[$i];
                                    if(isset($unreadMessages->$name)){
                                        $unMsgNumber = $unreadMessages->$name;
                                    }
                                    else {
                                        $unMsgNumber = "0";
                                    }
                                    
                                    echo "<li><a class=\"friend_links\" id=\"chat_link_friends\" href=\"chat.php?chatname=" . $name . "\">" . $name . "<span class=\"number_unread_messages\">" . $unMsgNumber . "</span></a></li>";
                                }
                            }
                        ?>
                    </ul>

                </div>
            </span>
            
        </div>

        <div id="cut_line_request">
            <h1 id="minor_header" class="left_sided_header">New Requests</h1>

            <ol>
                <?php
                    for($j = 0; $j < count($requested); $j = $j + 1) {
                        echo "<li>Friend request from <span id=\"request\">$requested[$j]</span></li><a href=\"friends.php?add_request=$requested[$j]\"><input type=\"button\" class=\"friend_request\" value=\"Add\"></a><a href=\"friends.php?decline_request=$requested[$j]\"><input type=\"button\" class=\"friend_request\" value=\"Decline\"></a>";
                    }
                ?>
            </ol>
        </div>

        <?php 
            if(isset($_POST["send_request"])) {
                if($errorMsg) {
                    echo "<p class=\"errorMsg\">Couldn't send friend request :( </p>";
                }
            }
        ?>

        <form id="sendRequests" method="post" action="friends.php">
            <div class="input_bar">
                <input name="requested_user_name" id="add_input" type="text" placeholder="Add Friend to List" onkeyup="findFriends(this)">
                <button type="submit" name="send_request" value="request" id="add_button">Add</button>
                <table id="guessTable"></table>
            </div>
        </form>
    </body>
</html>