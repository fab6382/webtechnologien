<?php
    require("start.php");
    session_unset();
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Logout</title>
        <link rel="stylesheet" href="style.css">
    </head>

    <body class="centered_element">
        <img class="picGeneral" src="../images/logout.png">
        
        <h1>Logged out...</h1>

        <p>See u!</p>

        <br>
        
        <a href="login.php">Login again</a>
    </body>
</html>