<?php
    require("start.php");
    //error_reporting(0);
    $usernameMsg = false;
    $passwordMsg = false;
    $password2Msg = false;

    if(isset($_POST['username'])) {
        $username = $_POST['username'];
        $password = $_POST['first_password'];
        $password2 = $_POST['second_password'];

        $usernameOk = false;
        $passwordOk = false;
        $password2Ok = false;
        $registration_error = false;

        $_SESSION["chat_token"] = null;

        if(strlen($username) >= 3 && $service->userExists($username) !== true) {
            $usernameOk = true;
        } else {
            $usernameMsg = true;
        }
        
        if(strlen($password) >= 8) {
            $passwordOk = true;
        } else {
            $passwordMsg = true;
        }

        if($password === $password2) {
            $password2Ok = true;
        } else {
            $password2Msg = true;
        }

        if($usernameOk && $passwordOk && $password2Ok) {
            if($service->register($username, $password)) {
                $_SESSION['user'] = $username;
                header('Location: friends.php');
            } else {
                $registration_error = true;
            }
        }
    }
?>

<!DOCTYPE html>

<html>
    <head>
        <title> Register </title>
        <link rel="stylesheet" href="style.css">
    </head>

    <body class="centered_element">
        <img class="picGeneral" src="../images/user.png">

        <h1> Register yourself </h1>

        <form id="registerForm" method="post" action="register.php">

            <span class="information_container">
                <fieldset id="register_block" class="input_block">
                    <legend id="register_block_name"> Register </legend>

                    <div class="input_bar">
                        <span class="input_left_part">
                            <label for="register_username"> Username </label>
                        </span>
                        <span class="input_right_part">
                            <input name="username" type="text" id="register_username" class="user_input_part" autocomplete="off" placeholder="Username">
                        </span>
                    </div>

                    <?php
                        if(isset($_POST['username'])){
                            if(strlen($username) < 3) {
                                echo "<p class=\"errorMsg\">Username is too short!</p>";
                            } else if($service->userExists($username) === true) {
                                echo "<p class=\"errorMsg\">Username is already taken!</p>";
                            }
                        }
                    ?>

                    <div class="input_bar">
                        <span class="input_left_part">
                            <label for="register_password_one"> Password </label>
                        </span>
                        <span class="input_right_part">
                            <input name="first_password" type="password" id="register_password_one" class="user_input_part" autocomplete="off" placeholder="Password">
                        </span>
                    </div>

                    <?php 
                        if(isset($_POST['username'])){
                            if($passwordMsg === true) {
                                echo "<p class=\"errorMsg\">Password too short (min. 8 characters)!</p>";
                            }
                        }
                    ?>

                    <div class="input_bar">
                        <span class="input_left_part">
                            <label for="register_password_two"> Confirm Password </label>
                        </span>
                        <span class="input_right_part">
                            <input name="second_password" type="password" id="register_password_two" class="user_input_part" autocomplete="off" placeholder="Confirm Password">
                        </span>
                    </div>

                    <?php 
                        if(isset($_POST['username'])){
                            if($password2Msg === true) {
                                echo "<p class=\"errorMsg\">Passwords do not match!</p>";
                            }
                        }
                    ?>
                </fieldset>
            </span>

            <div id="button_bar">
                <a href="login.php">
                    <input type="button" id="register_button" class="gray" value="Cancel">
                </a>

                <input type="submit" id="register_submit" class="blue" value="Create Account">
            </div>

            <?php 
                if(isset($_POST['username'])){
                    if($registration_error) {
                        echo "<p class=\"errorMsg\">Error while registration!</p>";
                    }
                }
            ?>
        </form>
    </body>
</html>