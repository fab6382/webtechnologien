<?php
    require("start.php");
    $showerror = false;
    if(isset($_SESSION["user"])) {
        header("Location: friends.php");
    }
    else {
        if(isset($_POST["username"])) {
            $username = $_POST['username'];
            $password = $_POST['password'];

            if($service->login($username, $password)) {
                $_SESSION["user"] = $username;
                header("Location: friends.php");
            }
            else {
                $showerror = true;
            }
        } 
    }
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" href="style.css">
    </head>

    <body class="centered_element">
        <img class="picGeneral" src="../images/chat.png">
        <h1>Please sign in</h1>

        <form id="loginForm" method="post" action="login.php">

            <span class="information_container">

            <fieldset id="login_block" class="input_block">
                <legend>Login</legend>

                <?php 
                    if(isset($_POST["username"])) {
                        if($showerror) {
                            echo "<p class=\"errorMsg\">Login failed! Maybe register first!</p>";
                        }
                    }
                ?>
                <div class="input_bar">
                    <span class="input_left_part">
                        <label for="login_username" class="user_information_label_part">Username</label>
                    </span>
                    <span class="input_right_part">
                        <input name="username" id="login_username" type="text" class="user_input_part" autocomplete="off" placeholder="Username" required></input>
                    </span>  
                </div>

                <div class="input_bar">
                    <span class="input_left_part">
                        <label for="login_password" class="user_information_label_part">Password</label>
                    </span>
                    <span class="input_right_part">
                        <input name="password" id="login_password" type="password" class="user_input_part" autocomplete="off" placeholder="Password" required></input>
                    </span>
                </div>
            </fieldset>

            </span>

            <div class="button_bar">
                <a href="register.php">
                    <input type="button" id="login_button" class="gray" value="Register">
                </a>

                <input type="submit" id="login_submit" class="blue" value="Login" default>
            </div>
        </form>
    </body>
</html>