<?php
    require("start.php");
    $service = new Utils\BackendService(CHAT_SERVER_URL, CHAT_SERVER_ID);
    var_dump($service->test());
    echo "<br>";
    var_dump($service->register("Test123", "12345678"));
    echo "<br>";
    var_dump($service->login("Test123", "12345678"));
    echo "<br>";
    var_dump($service->loadUser("Test123"));
    echo "<br>";
    var_dump($service->saveUser(new Model\User("Test123")));
    echo "<br>";
    var_dump($service->loadFriends());
    echo "<br>";
    var_dump($service->friendRequest(new Model\Friend("Test456")));
    echo "<br>";
    var_dump($service->friendAccept(new Model\Friend("Test456")));
    echo "<br>";
    var_dump($service->friendDismiss(new Model\Friend("Test456")));
    echo "<br>";
    var_dump($service->friendRemove(new Model\Friend("Test456")));
    echo "<br>";
    var_dump($service->userExists("Test456"));
    echo "<br>";
    var_dump($service->getUnread());
?>