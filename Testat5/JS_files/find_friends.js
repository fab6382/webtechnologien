var listOfAllUser = undefined;

var chatToken = window.chatToken;
var collectionId = window.chatCollectionId;
var server = window.chatServer;

var url = server + collectionId + '/user';
var unreadMessagesUrl = server + collectionId + '/unread';


var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        let data = JSON.parse(xmlhttp.responseText);
        listOfAllUser = data;
    }
};
xmlhttp.open("GET", url, true);
// Add token, e. g., from Tom
xmlhttp.setRequestHeader('Authorization', 'Bearer ' + chatToken);
xmlhttp.send();


var xmlhttpUnreadMessages = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        let data = JSON.parse(xmlhttp.responseText);
        updateMessageCount(data);
    }
};


function findFriends(element) {

    if(listOfAllUser === undefined) {
        alert('Something went wrong! Try again later!');
        return;
    }

    if(element instanceof HTMLInputElement) {
        const typedValue = element.value;
        const listOfGuessFriends = [];
        const guessTable = document.getElementById('guessTable');
        guessTable.innerHTML = '';

        for(let user of listOfAllUser) {
            if(typedValue !== '' && user.startsWith(typedValue) && user !== typedValue) {
                listOfGuessFriends.push(user);
            }
        }

        for(guessFriend of listOfGuessFriends) {
            
            const tableRow = document.createElement('tr');
            const tableElement = document.createElement('td');
            tableElement.innerText = guessFriend;
            tableElement.onclick = () => {
                element.value = tableElement.innerText;
                findFriends(element);
            };
            tableRow.appendChild(tableElement);
            guessTable.appendChild(tableRow);

            i++;
        }
    }
}

function updateMessageCount(data) {
    listedFriends = document.getElementsByClassName('friend_links');

    if(listedFriends instanceof HTMLCollection) {
        for(i = 0; i < listedFriends.length; i++) {
            username = listedFriends[i].innerText.split('\n')[0];
            try{
                number = data[username];
                if(number === undefined) {
                    throw new Error();
                }
                listedFriends[i].innerHTML = '' + username + '<span class=\"number_unread_messages\">' + number + '</span>';
            }
            catch(e) {
                listedFriends[i].innerHTML = '' + username + '<span class=\"number_unread_messages\">' + '0' + '</span>';
            }
        }
    }
}

window.setInterval(function () {
    xmlhttp.open("GET", unreadMessagesUrl, true);
    xmlhttp.setRequestHeader('Content-type', 'application/json');
    xmlhttp.setRequestHeader('Authorization', 'Bearer ' + chatToken);
    xmlhttp.send();
}, 1000);