//Collection ID und Token, damit kein Zugriff auf die Dummy-Seite notwendig ist
var collectionID = window.chatCollectionId;
var token = window.chatToken;
var server = window.chatServer;
var user = window.chatUser;

var url = server + collectionID + '/message';

//Send Messages definiert
let xmlhttpsend = new XMLHttpRequest();
xmlhttpsend.onreadystatechange = function () {
    if (xmlhttpsend.readyState == 4 && xmlhttpsend.status == 204) {
        console.log("done...");
    }
};

//List Messages definiert
var messages = 0;
var previousMessages = 0;

var xmlhttplist = new XMLHttpRequest();
xmlhttplist.onreadystatechange = function () {
    if (xmlhttplist.readyState == 4 && xmlhttplist.status == 200) {
        let data = JSON.parse(xmlhttplist.responseText);

        messages = data.length;

        //Für alle neuen Nachrichten iteriere durch Array aller jemals gesendeten Nachrichten und füge als neues Paragraph-Element ein
        for (i = previousMessages; i < messages; i++) {

            var time = new Date(data[i].time).toLocaleTimeString();

            console.log(time);

            const paragraph = document.createElement("p");
            paragraph.className = "chat_message";
            const rightSpan = document.createElement("span");
            rightSpan.className = "chat_message_time";
            const leftText = document.createTextNode(`${data[i].from}: ${data[i].msg}`);
            const rightText = document.createTextNode(time);
            rightSpan.appendChild(rightText);
            paragraph.appendChild(leftText);
            paragraph.appendChild(rightSpan);

            const element = document.getElementById("chat");
            element.prepend(paragraph);
        }

        previousMessages = messages;

    }
};

//Ausführen, sobald das Fenster geladen hat
window.onload = function () {

    //Alle bisherigen Nachrichten anzeigen
    xmlhttplist.open("GET", url + '/' + user, true);
    xmlhttplist.setRequestHeader('Authorization', 'Bearer ' + chatToken);
    xmlhttplist.send();

    //Drücken der Enter-Taste im Input-Feld zum Button-Click umwandeln
    document.getElementById("message_input").addEventListener("keyup", function (event) {

        if (event.key === "Enter") {
            document.getElementById("message_button").click();
        }

    });

    //Beim Drücken des Senden-Buttons die Nachricht aus dem Input-Feld absenden und das Input-Feld danach leeren
    document.getElementById("message_button").addEventListener("click", function () {

        xmlhttpsend.open("POST", url, true);
        xmlhttpsend.setRequestHeader('Content-type', 'application/json');
        xmlhttpsend.setRequestHeader('Authorization', 'Bearer ' + chatToken);

        let data = {
            message: document.getElementById("message_input").value,
            to: user
        };
        let jsonString = JSON.stringify(data);
        xmlhttpsend.send(jsonString);

        document.getElementById("message_input").value = null;

    });
}

//Jede Sekunde aktualisieren
window.setInterval(function () {
    xmlhttplist.open("GET", url + '/' + user, true);
    xmlhttplist.setRequestHeader('Authorization', 'Bearer ' + chatToken);
    xmlhttplist.send();
}, 1000);