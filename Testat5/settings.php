<?php
    require("start.php");

    if (!isset($_SESSION["user"])) {
        header("Location: login.php");
        exit();
    }

    $currentUser = $service->loadUser($_SESSION["user"]);

    $fehlermeldungen = array();
    $firstName = $currentUser->getFirstName();
    $lastName = $currentUser->getLastName();
    $coffeeOrTea = $currentUser->getCoffeeOrTea();
    $description = $currentUser->getDescription();
    $layout = $currentUser->getLayout();

    if ($firstName == "") {
        $firstName = $currentUser->getUsername();
    }

    if ($coffeeOrTea == "") {
        $coffeeOrTea = "3";
    }

    if ($layout == "") {
        $layout = "1";
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $action = $_POST["saveButton"];
        if ($action == "send") {
            $firstName = $_POST["firstName"];
            $lastName = $_POST["lastName"];
            $coffeeOrTea = $_POST["coffeeOrTea"];
            $description = $_POST["description"];
            $layout = $_POST["layout"];

            if ($firstName == "") {
                $fehlermeldungen[] = "Please enter your first name";
            }

            if ($lastName == "") {
                $fehlermeldungen[] = "Please enter your last name";
            }

            if ($coffeeOrTea == "") {
                $fehlermeldungen[] = "Please tell us your favourite drink";
            }

            if ($description == "") {
                $fehlermeldungen[] = "Please tell us more about yourself";
            }

            if ($layout == "") {
                $fehlermeldungen[] = "Please choose your prefered chat layout";
            }

            if (count($fehlermeldungen) == 0) {
                $currentUser->setFirstName($firstName);
                $currentUser->setLastName($lastName);
                $currentUser->setCoffeeOrTea($coffeeOrTea);
                $currentUser->setDescription($description);
                $currentUser->setLayout($layout);
                $service->saveUser($currentUser);
                header("Location: friends.php");
                exit();
            }
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Profile Settings</title>
        <link rel="stylesheet" href="style.css">
    </head>

    <body class="simple_centered_element">

        <h1 class="left_sided_header">Profile Settings</h1>

        <form name="settings" action="" method="post">

            <span class="information_container">
                <fieldset id="base_settings" class="input_block">

                    <legend>Base Data</legend>

                    <span class="centered_element">

                        <div class="input_bar">
                            <span class="input_left_part">
                                <label class="user_information_label_part">First Name</label> 
                            </span>
                            <span class="input_right_part">
                                <input type="text" class="user_input_part" placeholder="Your name" id="firstName" name="firstName" value="<?= $firstName ?>">
                            </span>
                        </div>

                        <div class="input_bar">
                            <span class="input_left_part">
                                <label class="user_information_label_part">Last Name</label> 
                            </span>
                            <span class="input_right_part">
                                <input type="text" class="user_input_part" placeholder="Your surname" id="lastName" name="lastName" value="<?= $lastName ?>">
                            </span>
                        </div>

                        <div class="input_bar">
                            <span class="input_left_part">
                                <label class="user_information_label_part">Coffee or Tea?</label> 
                            </span>
                            <span class="input_right_part">
                                <select class="user_input_part" id="coffeeOrTea" name="coffeeOrTea">
                                    <option value="1" <?php if ($coffeeOrTea == "1") {echo 'selected="selected"';}?>>Coffee</option>
                                    <option value="2" <?php if ($coffeeOrTea == "2") {echo 'selected="selected"';}?>>Tea</option>
                                    <option value="3" <?php if ($coffeeOrTea == "3") {echo 'selected="selected"';}?>>Neither nor</option>
                                </select>
                            </span>
                        </div>

                    </span>

                </fieldset>
            </span>

            <br>

            <span class="information_container">
                <fieldset id="additional_settings">

                    <legend>Tell Something About You</legend>
                    <textarea id="settings_textarea" class="user_input_part" placeholder="Leave a comment here" id="description" name="description"><?= $description ?></textarea>

                </fieldset>
            </span>

            <br>

            <span class="information_container">
                <fieldset id="chat_settings">

                    <legend>Prefered Chat Layout</legend>
                    <div class="special_input_bar">
                        <input type="radio" id="oneLineLayout" name="layout" value="1" <?php if ($layout == "1") {echo 'checked="checked"';}?>>
                        <label for="oneLineLayout">Username and message in one line</label>
                    </div>
                    <div class="special_input_bar">
                        <input type="radio" id="seperateLineLayout" name="layout" value="2" <?php if ($layout == "2") {echo 'checked="checked"';}?>>
                        <label for="seperateLineLayout">Username and message in seperated lines</label>
                    </div>

                </fieldset>
            </span>

            <br>

            <?php
                if (count($fehlermeldungen) > 0) {
            ?>
            <p>
                <ul>
                    <?php
                        foreach($fehlermeldungen as $fehlermeldung) {
                            echo "<li>" . $fehlermeldung . "</li>";
                        }
                    ?>
                </ul>
            </p>
            <?php
                }
            ?>

            <span class="centered_element">
                <div>
                    <a href="friends.php">
                        <button class="gray" type="button">Cancel</button>
                    </a> 
                    <button class="blue" type="submit" name="saveButton" value="send">Save</button>
                </div>
            </span>

        </form>

    </body>
</html>