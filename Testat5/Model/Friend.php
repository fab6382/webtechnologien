<?php
    namespace Model;
    use JsonSerializable;

    class Friend implements JsonSerializable {
        private $username;
        private $status;

        public function __construct($username = null) {
            $this->username = $username;
        }

        public function getUsername() {
            return $this->username;
        }

        public function getStatus() {
            return $this->status;
        }

        public function setStatusAccepted() {
            $this->status = "accepted";
        }

        public function setStatusDismissed() {
            $this->status = "dismissed";
        }

        public static function fromJson($data){
            //$user = new User();
            $friend = new Friend();
            foreach ($data as $key => $value) {
                //$user->{$key} = $value;
                if($key === "username") {
                    $friend = new Friend($value);
                }
                else if($key === "status") {
                    if($value === "accepted") {
                        $friend->setStatusAccepted();
                    }
                    else if($value === "dismissed") {
                        $friend->setStatusDismissed();
                    }
                }
            }
            //return $user;
            return $friend;
        }

        public function jsonSerialize() {
            return get_object_vars($this);
        }            
    }
?>