<?php
    namespace Model;
    use JsonSerializable;

    class User implements JsonSerializable {
        private $username;
        private $firstName;
        private $lastName;
        private $coffeeOrTea;
        private $description;
        private $layout;

        public function __construct($username = null) {
            $this->username = $username;
        }

        public function getUsername() {
            return $this->username;
        }

        public function setFirstName($name) {
            $this->firstName = $name;
        }

        public function getFirstName() {
            return $this->firstName;
        }

        public function setLastName($name) {
            $this->lastName = $name;
        }

        public function getLastName() {
            return $this->lastName;
        }

        public function setCoffeeOrTea($preference) {
            $this->coffeeOrTea = $preference;
        }

        public function getCoffeeOrTea() {
            return $this->coffeeOrTea;
        }

        public function setDescription($description) {
            $this->description = $description;
        }

        public function getDescription() {
            return $this->description;
        }

        public function setLayout($layout) {
            $this->layout = $layout;
        }

        public function getLayout() {
            return $this->layout;
        }

        public static function fromJson($data){
            $user = new User();
            foreach ($data as $key => $value) {
                $user->{$key} = $value;
            }
            return $user;
        }

        public function jsonSerialize() {
            return get_object_vars($this);
        }            
    }
?>