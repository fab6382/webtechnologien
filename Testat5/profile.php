<?php
    require("start.php");

    if (!isset($_SESSION["user"])) {
        header("Location: login.php");
        exit();
    }

    $currentUser = $service->loadUser($_SESSION["user"]);

    if (!isset($_GET["friend"])) {
        header("Location: friends.php");
        exit();
    } else {
        $currentFriend = $service->loadUser($_GET["friend"]);
    }

    $userName = $currentFriend->getUsername();
    $firstName = $currentFriend->getFirstName();
    $lastName = $currentFriend->getLastName();
    $coffeeOrTea = $currentFriend->getCoffeeOrTea();
    $description = $currentFriend->getDescription();
    $fullName;

    if (!$firstName) {
        $firstName = $userName;
    }

    if (!$lastName) {
        $fullName = $firstName;
    } else {
        $fullName = $firstName . " " . $lastName;
    }

    if ($coffeeOrTea == "1") {
        $coffeeOrTea = "Coffee";
    } else {
        if ($coffeeOrTea == "2") {
            $coffeeOrTea = "Tea";
        } else{
            if ($coffeeOrTea == "3") {
                $coffeeOrTea = "Neither nor";
            } else {
                $coffeeOrTea = "Not selected";
            }
        }
    }

    if (!$description) {
        $description = "No description yet";
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Profile of <?=$userName?></title>
        <link rel="stylesheet" href="style.css">
    </head>

    <body class="simple_centered_element">
        <h1>Profile of <?=$userName?></h1>

        <div>
            <a href="chat.php?chatname=<?=$currentFriend->getUsername()?>">&lt; Back to Chat</a> 
            | 
            <a id="remove" href="friends.php?remove=<?=$currentFriend->getUsername()?>">Remove Friend</a>
        </div>

        <br>
        
        <div class="flex-container">
            <div id="pic_description">
                <img id="profilePic" src="../images/profile.png" alt="profile picture">
            </div>
            
            <div id="description" class="information_container">

                <?=$description?>
                
                </br>

                <dl>
                    <dt>Coffe or Tea?</dt>
                    <dd><?=$coffeeOrTea?></dd>
                    <dt>Name</dt>
                    <dd><?=$fullName?></dd>
                </dl>

            </div>
        </div>
    </body>
</html>