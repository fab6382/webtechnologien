# webtechnologien
Mega krasse Interwebz Seite

# Arbeitsweise:
Wäre gut, wenn jeder seinen Teil der Arbeit in einem eigenen Branch erstellt (git checkout -b branchname) und dann, wenn er fertig ist in den main Branch reinmerged.
Dadurch können wir verhindern, dass jemand mit seinen Änderungen die Arbeit des anderen überschreibt, da bei jedem Merge der Code erst aufeinander angepasst werden muss.

# Anleitung:
1. Wenn eine neue Aufgabe angefangen wird muss zuerst auf den Hauptbranch gewechselt werden -> git checkout main
2. Dann wird sich der neue Stand des Repositories mit den Änderungen der anderen Teammitgliedern geholt -> git pull
3. Dann wechelt man auf einen eigenen Branch, auf welchem man isoliert von den Anderen arbeiten kann, ohne eventuelle Konflikte zu verursachen -> git checkout -b branchname
4. Wenn man seine Änderungen pushen will einfach -> git push (beim ersten mal muss der upstream auf den richtigen Branch gewechselt werden, der genaue Befehl wie man das macht wird einem im Terminal in VS COde angezeigt, wenn man das erste mal git push in seinem neuen Branch macht).
5. Wenn die Aufgabe fertig ist und in den main Branch integriert werden soll, müssen vorher die Änderungen der Anderen geholt und verarbeitet werden. Da nämlich in der Zwischenzeit andere Teammitglieder ihre Branches in den main Branch gemerged hätten können, würde einem selber diese Änderungen (Commits) fehlen. Das macht man indem man zuerst den aktuellen Stand des main Branches holt -> git fetch origin main
6. Dann integriert man den geholten Stand in den eigenen Branch -> git rebase origin/main
7. Falls beim rebasen Konflikte in Dateien entstehen wird das einem in VS Code angezeigt. Diese Konflikte behebt man dann und fährt fort, wie es einem im Terminal angeleitet wird
8. Dann pusht man seinen Branch erneut mit -> git push --force
9. Um seinen Branch dann in den main Branch zu integrieren geht man über den Browser auf BitBucket und dort ins Repository. Dort gibt es auf der linken Seite eine Schaltfläche, wo an 4. Stelle von oben "Pull requests" steht.  Dort drück man dann auf den Button "Create Pull request" und füllt das Formular wie angegeben aus. Dann hat man die Möglichkeit nochmal den Code anzuschauen, um auch sicher zu gehen, dass alles so passt wie es ist. Ob man selber dann die Branches zusammenfügen kann oder das ein anderes Teammitglied machen muss weiß ich nicht, ist aber an sich ja kein allzu großes Problem.
10. Wenn der Branch erfolgreich in den main Brnach integriert wurde, kann der frühere Branch gelöscht werden und man fängt wiedre bei 1. an
