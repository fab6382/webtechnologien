import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from 'src/app/services/backend.service';
import { ContextService } from 'src/app/services/context.service';
import { Profile } from 'src/app/models/Profile';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
    public firstName!: string;
    public lastName!: string;
    public coffeeOrTea!: string;
    public description!: string;
    public layout!: string;
    public profile!: Profile;

    public constructor(private backendService: BackendService, private router: Router, private contextService: ContextService) {
    }

    public ngOnInit(): void {
        this.getSettings();
    }

    public getSettings(): void {
        this.backendService.loadCurrentUser().then((user): void => {
            if (user == null) {
                if (confirm("Sie sind nicht angemeldet. Wollen Sie zurück zur Login-Seite?")) {
                    this.router.navigate(['/login']);
                }
            } else {
                let userObject;
                try {
                    userObject = JSON.parse(JSON.stringify(user))
                } catch (error) {
                    console.error(error)
                }
                for (let key in userObject) {
                    let value = userObject[key];
                    if (key == "firstName") {
                        this.firstName = value;
                    }
                    if (key == "lastName") {
                        this.lastName = value;
                    }
                    if (key == "coffeeOrTea") {
                        this.coffeeOrTea = value;
                    }
                    if (key == "description") {
                        this.description = value;
                    }
                    if (key == "layout") {
                        this.layout = value;
                    }
                }
                if (this.coffeeOrTea == null || this.coffeeOrTea == "") {
                    this.coffeeOrTea = "3";
                }
                if (this.layout == null || this.layout == "") {
                    this.layout = "1";
                }
                if (this.firstName == null || this.firstName == "") {
                    this.firstName = this.contextService.loggedInUsername;
                }
                this.profile = new Profile(this.firstName, this.lastName, this.coffeeOrTea,this.description, this.layout);
            }
        });

    }

    public submitSettings(): void {
        this.profile = new Profile(this.firstName, this.lastName, this.coffeeOrTea, this.description, this.layout);
        console.log(this.profile);
        this.backendService.saveCurrentUserProfile(this.profile);
        console.log(this.backendService.loadCurrentUser());
        this.router.navigate(['/friends']);
    }
}
