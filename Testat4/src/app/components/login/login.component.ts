import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { BackendService } from 'src/app/services/backend.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    public msgLogin: boolean = true ;
    public username: string = '';
    public pass: string = '';

    public constructor(private router: Router, private backend: BackendService) { 
    }

    public ngOnInit(): void {
    }
    
    public login(): void {
        this.backend.login(this.username, this.pass)
            .then((ok: boolean) => {
                if(ok) {
                    console.log('Successeful login!')
                    this.router.navigate(['friends']);
                } else {
                    this.msgLogin = false;
                }
            })
            .catch(() => {
                console.log('Error while User check!');
            });
    }

    public registrieren(): void {
        this.router.navigate(['register']);
    }
}
