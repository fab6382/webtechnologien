import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from 'src/app/services/backend.service';
import { ContextService } from 'src/app/services/context.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    public username: string = '';
    public pass: string = '';
    public pass2: string = '';
    public errorUseMsg: string = '';
    public passwordOK: boolean = false;
    public password2OK: boolean = false;
    public usernameOK: boolean = false;
    public msgPassword: boolean = true;
    public msgPassword2: boolean = true;
    public msgUsername: boolean = true;


    public constructor(private router: Router, private backendService: BackendService) { 
    }

    public ngOnInit(): void {
    }

    public checkUsername(): void {
        if(this.username.length >= 3) {
           this.backendService.userExists(this.username)
            .then((taken: boolean) => {
                if(taken) {
                    this.usernameOK = false;
                    this.errorUseMsg = 'Username is already taken!';
                    this.msgUsername = false;
                } else {
                    this.usernameOK = true;
                    this.msgUsername = true;
                }
            })
            .catch(() => {
                console.log('Error while checkUsername()');
            });
        } else {
            this.usernameOK = false;
            this.errorUseMsg = 'Username is too short!';
            this.msgUsername = false;  
        }
    }

    public checkPassword(): void {
        if(this.pass.length >= 8) {
            this.passwordOK = true;
            this.msgPassword = true;
        } else {
            this.passwordOK = false;
            this.msgPassword = false;
        }
    }

    public checkPassword2(): void {
        if(this.pass2 === this.pass) {
            this.password2OK = true;
            this.msgPassword2 = true;
        } else {
            this.password2OK = false;
            this.msgPassword2 = false;
        }
    }

    public createButton(): void {
        this.backendService.register(this.username, this.pass)
            .catch(() => {
                console.log('Error while registration!');
            });
        
        this.backendService.login(this.username, this.pass)
            .then((ok: boolean) => {
                if(ok) {
                    console.log('Successeful login!')
                    this.router.navigate(['friends']);
                } else {
                    console.log('Something went wrong trying to log in!');
                }
            })
            .catch(() => {
                console.log('Error while User check!');
            });
    }

    public abbrechen(): void {
        this.router.navigate(['login']);
    }
}
