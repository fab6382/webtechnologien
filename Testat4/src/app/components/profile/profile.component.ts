import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { BackendService } from 'src/app/services/backend.service';
import { ContextService } from 'src/app/services/context.service';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    public headerContent: string = '';
    public description: string = '<No entry yet>';
    public name: string = '<No entry yet>';
    private firstName: string = '';
    private lastName: string = '';
    public teaOrCoffee: string = '<No entry yet>';

    public constructor(private router: Router, private backend: BackendService, private context: ContextService) { 
    }

    public ngOnInit(): void {
        this.headerContent = 'Profile of ' + this.context.currentChatUsername;

        this.backend.loadUser(this.context.currentChatUsername).then( (user) => {
            if(user === null) {
                alert('Something went wrong! Redirecting to Login!');
                this.router.navigate(['login']);
            }
            else {
                console.log(user);
                const userObject = {...user} as any;

                Object.keys(userObject).forEach( (key: string) => {
                    switch(key) {
                        case 'description': 
                            this.description = userObject[key];
                            break;

                        case 'lastName':
                            this.lastName = userObject[key];
                            break;

                        case 'firstName':
                            this.firstName = userObject[key];
                            break;

                        case 'coffeeOrTea':
                            const coffeeTea = userObject[key];
                            switch(coffeeTea) {
                                case '1': 
                                    this.teaOrCoffee = 'Coffee';
                                    break;

                                case '2':
                                    this.teaOrCoffee = 'Tea';
                                    break;

                                case '3':
                                    this.teaOrCoffee = 'Neither nor';
                                    break;

                                default:
                                    // Do nothing
                            }
                            break;

                        default:
                            // Do nothing
                    }
                });

                if(this.firstName !== '' || this.lastName !== ''){
                    this.name = this.firstName + ' ' + this.lastName;
                }
            }
        });
    }


    public redirectToChat(): void {
        this.router.navigate(['chat']);
    }

    public removeFriend(): void {
        if(confirm('Do you really want to remove ' + this.context.currentChatUsername + ' as friend?')) {
            this.backend.removeFriend(this.context.currentChatUsername);
            this.router.navigate(['friends']);
        }
    }
}
