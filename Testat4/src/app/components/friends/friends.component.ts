import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Friend } from 'src/app/models/Friend';
import { User } from 'src/app/models/User';
import { BackendService } from 'src/app/services/backend.service';
import { ContextService } from 'src/app/services/context.service';
import { IntervalService } from 'src/app/services/interval.service';

@Component({
    selector: 'app-friends',
    templateUrl: './friends.component.html',
    styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

    private currentUser: User = <User>{};
    public usersFriendRequests: Array<string> = [];
    private usersFriendRequestsBuffer: Array<string> = [];
    public usersFriends: Array<Friend> = [];
    private usersFriendsBuffer: Array<Friend> = []
    public headerContent: string = '';
    public requestUsername: string = '';

    public constructor(private router: Router, private context: ContextService, private interval: IntervalService, private backend: BackendService) {
    }

    public ngOnInit(): void {
        this.headerContent = this.context.loggedInUsername + '\'s Friends';

        this.interval.clearIntervals();

        this.interval.setInterval('FriendsComponent', () => {
            this.getData();
        });
    }

    private getData(): void {
        this.backend.loadCurrentUser().then( (value) => {
            this.currentUser = value as User;
        });

        this.usersFriendsBuffer = [];
        this.usersFriendRequestsBuffer = [];

        this.backend.loadFriends().then( (friends: Array<Friend>) => {
            for(let friend of friends) {
                if(friend.status == 'accepted') {
                    friend.unreadMessages = 0;
                    this.usersFriendsBuffer.push(friend);
                }
                else{
                    this.usersFriendRequestsBuffer.push(friend.username);
                }
            }

            this.backend.unreadMessageCounts().then( (value) => {
                for (let i = 0; i < this.usersFriends.length; i++) {
                    if(value.has(this.usersFriends[i].username)) {
                        this.usersFriends[i].unreadMessages = value.get(this.usersFriends[i].username) as number;
                    }
                }
            });

            this.usersFriends = this.usersFriendsBuffer;
            this.usersFriendRequests = this.usersFriendRequestsBuffer;
        });
    }

    public acceptRequest(name: string): void {
        this.backend.acceptFriendRequest(name);
    } 

    public declineRequest(name: string): void {
        this.backend.dismissFriendRequest(name);
    }

    public sendRequest(): void {
        this.backend.userExists(this.requestUsername).then((value: boolean) => {
            if(value) {
                alert('Request is sent!');
                this.backend.friendRequest(this.requestUsername);
            }
            else {
                alert('User does not exist!');
            }
        });
    }

    public redirectToLogout(): void {
        this.router.navigate(['logout']);
    }

    public redirectToSettings(): void {
        this.router.navigate(['settings']);
    }

    public redirectToChat(username: string): void {
        this.context.currentChatUsername = username;
        this.router.navigate(['chat']);
    }
}
