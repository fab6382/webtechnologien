import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { IntervalService } from 'src/app/services/interval.service';
import { ContextService } from 'src/app/services/context.service';
import { Message } from 'src/app/models/Message';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit, AfterViewChecked {
    @ViewChild('messagesDiv') private myScrollContainer: ElementRef;
    public messageInput!: string;
    public messages!: Array<Message>;
    public currentChatPartner!: string;
    public currentUser!: User;
    public layoutSetting!: string;

     public constructor(private backendService: BackendService, private intervalService: IntervalService, private router: Router, private contextService: ContextService) { 
        this.myScrollContainer = new ElementRef(null);
        this.currentChatPartner = contextService.currentChatUsername;
    }

    public ngAfterViewChecked() {        
        this.scrollToBottom();
    } 

    private scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { 

        }                 
    }

    public ngOnInit(): void {
        this.getLayout();
        this.getMessages();
        this.scrollToBottom();
        this.intervalService.setInterval("chat.component.ts", () => {this.getMessages();});
    }

    public removeFriend(): void {
        if(confirm("Do you really want to remove " + this.currentChatPartner + " as a friend?")) {
            this.backendService.removeFriend(this.currentChatPartner);
            this.router.navigate(['/friends'])
        } else {
            alert("User " + this.currentChatPartner + " was not removed as a friend");
        }
    }

    public getLayout(): void {
        this.backendService.loadCurrentUser().then((user) => {
            if (user == null) {
                if (confirm("You are not logged in. Do you want to return to the login page?")) {
                    this.router.navigate(['/login']);
                }
            } else {
                this.currentUser = user;
                let userObject;
                try {
                    userObject = JSON.parse(JSON.stringify(this.currentUser))
                } catch (error) {
                    console.error(error)
                }
                for (let key in userObject) {
                    let value = userObject[key];
                    if (key == "layout") {
                        this.layoutSetting = value;
                    }
                }
                if (this.layoutSetting == null) {
                    this.layoutSetting = "1";
                }
            }
        });
    }

    public sendMessage(): void {
        this.backendService.sendMessage(this.currentChatPartner, this.messageInput);
        this.messageInput = "";
        this.getMessages();
    }

    public getMessages(): void {
        let chatBox = document.getElementById("chat");
        if (!chatBox) {
            return;
        }
        this.backendService.listMessages(this.currentChatPartner).then((messageList) => {
            if (messageList.length > 0) {
                this.messages = messageList;
            } else {
                console.warn("Fehler oder keine Nachrichten vorhanden");
            }
        });
    }

}